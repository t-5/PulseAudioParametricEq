from datetime import datetime
from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QDesktopServices
from designer_qt5.Ui_AboutDialog import Ui_AboutDialog
from helpers.constants import *
from PyQt5.QtWidgets import QDialog
import os
import sys


class AboutDialog(QDialog, Ui_AboutDialog):
    def __init__(self):
        super(AboutDialog, self).__init__()
        self.setupUi(self)
        self.label.setText(self.label.text().replace("$version", CURRENT_VERSION))
        self.label_2.setText(self.label_2.text().replace("$year", datetime.now().strftime("%Y")))
        self.label_4.mousePressEvent = self.onEmailClicked
        self.label_5.mousePressEvent = self.onLicenceClicked
        self.setModal(True)


    # noinspection PyUnusedLocal,PyMethodMayBeStatic
    def onEmailClicked(self, ignore=None):
        url = QUrl('mailto:t-5@t-5.eu')
        QDesktopServices().openUrl(url)


    # noinspection PyUnusedLocal,PyMethodMayBeStatic
    def onLicenceClicked(self, ignore=None):
        url = QUrl(os.path.join(os.path.dirname(sys.argv[0]), "licence.txt"))
        QDesktopServices().openUrl(url)
