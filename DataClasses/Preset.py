from helpers.constants import *
import json


class Preset(object):
    def __init__(self):
        self.name = "Flat"
        self.gain = 0.0
        self.frequencyLow = FREQUENCY_DEFAULTS["Low"]
        self.frequencyParam1 = FREQUENCY_DEFAULTS["Param1"]
        self.frequencyParam2 = FREQUENCY_DEFAULTS["Param2"]
        self.frequencyParam3 = FREQUENCY_DEFAULTS["Param3"]
        self.frequencyHigh = FREQUENCY_DEFAULTS["High"]
        self.gainLow = 0.0
        self.gainParam1 = 0.0
        self.gainParam2 = 0.0
        self.gainParam3 = 0.0
        self.gainHigh = 0.0
        self.qLow = 1.0
        self.qParam1 = 1.0
        self.qParam2 = 1.0
        self.qParam3 = 1.0
        self.qHigh = 1.0


    def serialize(self):
        """ serialize to json string """
        d = {}
        for suffix in BAND_NAMES:
            d["frequency%s" % suffix] = getattr(self, "frequency%s" % suffix)
            d["gain%s" % suffix] = getattr(self, "gain%s" % suffix)
            d["q%s" % suffix] = getattr(self, "q%s" % suffix)
        d["gain"] = self.gain
        d["name"] = self.name
        return json.dumps(d)


    def unserialize(self, s):
        """ unserialize from json string """
        d = json.loads(s)
        for k, v in d.items():
            setattr(self, k, v)
